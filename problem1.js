let fs = require('fs')

function readingData(){
    return new Promise((resolve, reject) =>{
        fs.readFile('./data.json', 'utf8', (error, data) =>{
            if (error){
                reject(error)
            }
            else{
                resolve(JSON.parse(data))
            }
        })
    }) 
}

function retrievingData(data){
    let array = data["employees"]
    let result = array.filter((employee)=>{
        return employee.id === 2 || employee.id === 13 || employee.id === 23
    })
    console.log(result)
    return data
}

function groupingData(data){
    let array = data["employees"]
    let result = array.reduce((accumilator, employee)=>{
       if(accumilator.hasOwnProperty(employee.company)){
          accumilator[employee.company].push({'id': employee.id, 'name':employee.name})
       } else{
          accumilator[employee.company] = []
          accumilator[employee.company].push({'id': employee.id, 'name':employee.name})
       }
       return accumilator
    }, {})
    console.log(result)
    return data
}

function getDataForCompany(data){
    let array = data["employees"]
    let result = array.filter((employee)=>{
        return employee.company === "Powerpuff Brigade"
    })
    console.log(result)
    return data
}

function removingtheId(data){
    let array = data["employees"]
    let result = array.filter((employee)=>{
        return employee.id !== 2
    })
    let output = {'employees': result}
    console.log(output)
    return output
}

function sortingTheData(data){
    let array = data["employees"]
    let result = array.sort((emp1, emp2)=>{
        if (emp1.company > emp2.company){
            return 1
        } else if (emp1.company < emp2.company){
            return -1
        } else {
            if (emp1.id > emp2.id){
                return 1
            } else{
                return -1
            }
        }
    })
    console.log(result)
    return {'employees': result}
}

readingData()
.then((data)=>{
    return data
})
.then((data)=>{
    return retrievingData(data)
})
.then((data)=>{
    return groupingData(data)
})
.then((data)=>{
    return getDataForCompany(data)
})
.then((data)=>{
    return removingtheId(data)
})
.then((data)=>{
    return sortingTheData(data)
})
.catch((error)=>{
    console.log("Error is: ", error)
})